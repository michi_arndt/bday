import { Fan } from './../interfaces/fan';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FanService {
  fans: Array<Fan> = [
    {
      name: "André Wolfschmitt",
      img: "andre_wolfschmitt.png",
      text: "Michi, alter Kumpel! Ich wünsche dir von Herzen nur das Beste zu deinem 30. Geburtstag. Wir kennen uns seid der Grundschule und haben seitdem viele geile Zeiten zusammen verbracht. So eine Freundschaft ist nicht selbstverständlich und ich freue mich sehr, dass wir wieder so oft gemeinsam unterwegs sind. Hoffe auf noch viele Abende, Partys, Gespräche an der Isar, Tretbootfahrten, Urlaube und Städtetrips mit dir."
    },
    {
      name: "Johannes Dettelbacher",
      img: "johannes_dettelbacher.png",
      text: "Egal ob Businesslunches, Falafelläden scouten, Karnevaltrips, Gewichte bewegen, stumpf den Helm lackieren, 2. kicken (oder auch 1.), Hotelbuffets abklappern, Berge erklimmen und vieles mehr. Alles ist ein tolles Erlebnis mit dir. Ich wünsch dir alles gute und hoffe, dass noch viele weitere gemeinsame Abenteuer auf uns warten."
    },
    {
      name: "Jonas Cron",
      img: "jonas_cron.png",
      text: "Lieber Michi, zu deinem 30. wünsche ich dir alles Gute und alles Glück der Welt. Mit dir Zeit zu verbringen heißt auch immer Spaß zu haben aber du hast auch immer ein offenes Ohr für ein tiefgründiges und ernsthaftes Gespräch. Ich bin froh dich als einen meiner besten Freunde zu haben ❤️ auf die nächsten 30 😘"
    },
    {
      name: "Florian Cron",
      img: "flo_cron.png",
      text: "Michael mein Jung. Ich wünsche dir von Herzen alles Liebe zum 30. Geburtstag. Hoffe das du irgendwann wieder den Weg in den Landkreis findest weil mit so einem geilen Typen will man eigentlich mehr Zeit verbringen. Habe mich für das Bild entschieden weil ich auf viele weitere Trips mit dir hoffe. In Love 😘"
    },
    {
      name: "Stefan Meinert",
      img: "stefan_meinert.png",
      text: "Liebe auf den zweiten Blick: Als im Sommer 2010 ein 17 jähriger Schulbuu beim ASV vor mir stand, hätte ich im Traum nicht daran gedacht, das sich mit dir einmal eine echte Freundschaft entwickelt. Ich freue mich immer wieder, wenn es dich in die Heimat zieht und hoffe noch auf zahlreiche Einsätze mit dir😄. Ohne Gewähr, müsste dieses Bild nach einem langen Kirchweih Montag im Jahre 2014 gegen 9 Uhr früh entstanden sein, Michael gönnt sich vor dem Schlafen gehen noch einen Fleischsalat vom Oeder. Für mich war leider kein Platz mehr auf dem Bild. Happy Birthday😊"
    },
    {
      name: "Michelle Cron",
      img: "michelle_cron.png",
      text: "Lieber Michi, wir kennen uns schon so lange. In der Zeit in der ich dich kenne, habe ich dich als fürsorglichen, lustigen und lebhaften Menschen erlebt. Dich als Freund zu haben, ist wirklich etwas besonderes. Neben dem ganzen Unsinn, den man wahrhaft mit dir machen kann, neben unendlichen Bierpong-Tunieren, kann man auch immer tiefgründig mit dir reden! Du bist ein ehrlicher, charismatischer Mensch :). Behalte deine lebensfrohe Art. Happy Birthday ❤️"
    },
    {
      name: "Ivonne Reich",
      img: "ivonne_reich.png",
      text: "Lieber Michi, Ich wünsche dir von Herzen alles alles Liebe und Gute zum 30. Geburtstag. Du gehörst eindeutig zu meinen Lieblingsmenschen und ich bin schon sehr gespannt, was wir alles in den nächsten Jahren erleben ❤️ Daher: Auf unsere Freundschaft, auf weitere tolle, spaßige und herzvolle Erinnerungen, AUF DICH! Lass krachen und bleib so wie du bist 😘"
    },
    {
      name: "Jan Rottmann",
      img: "jan_rottmann.png",
      text: "Ach Michi, was haben wir nicht schon alles zusammen erlebt. Seien es die zahlreichen Urlaube, geile Feiern oder einfach ein gechillter Abend, alles ist mit dir eine Klasse besser. Ich hoffe, es folgen noch zahlreiche weitere Erlebnisse mit dir. Und auch wenn wir uns manchmal über längere Zeit nicht sehen, fühlt sich jedes Wiedersehen an, als wenn wir uns erst den Tag vorher getroffen hätten. Ich finde das macht wahre Freundschaft aus. In diesem Sinne wünsche ich dir von Herzen alles Gute zu deinem Geburtstag. Auf dich 😘"
    },
    {
      name: "Julia Wolfschmitt",
      img: "julia_wolfschmitt.png",
      text: "Wir kennen uns so lange, dass du mich schon geschimpft hast, als ich noch zu jung für die Klopfer und Zigaretten in meiner Tasche war. Du bist mit deiner offenen, positiven Art eine Bereicherung für jeden Anlass und ein reflektierter, herzlicher Mensch. Deshalb freu ich mich umso mehr, dass wir das ein oder andere Fest zusammen feiern und du endlich zum richtigen Verein zurückgewechselt bist 😉 alles Liebe zum Geburtstag und feier ordentlich! Auf dich Michi 😘"
    },
    {
      name: "Stefan Wild",
      img: "stefan_wild.png",
      text: "Lieber Michi alter Malle-Cowboy. Uns verbindet ja auch eine besondere Freundschaft. Wir sehen uns nicht all zu oft, aber wenn wir uns sehen, haben wir immer eine rießen Gaudi zusammen. 😀 Unsere Mallotze Feiern würde ich auf jeden Fall als das absolute Highlight herausstellen💪 Es freut mich sehr dich kennen gelernt zu haben, und bei deinem 30ten Geburtstag dabei sein zu können. Bleib so wie du bist. In dem Sinne, ein Hoch auf Dich, und alles alles Gute. Gruß Wildi"
    },
    {
      name: "Jens Rottmann",
      img: "jens_rottmann.png",
      text: "Lieber Michi, wir kennen uns schon seit klein auf. Man hat in den Jahren mal mehr und mal weniger Zeit miteinander verbracht. Aktuell, zum großen Bedauern, leider zu wenig, aber es ist jedes Mal ein absolutes Highlight mit dir etwas zu unternehmen. Alleine schon wenn ich an unsere Kaiserabende zu zweit denke 😉 Ich hoffe es wird immer so bleiben, mit der Ausnahme, dass wir uns wieder häufiger sehen. Bleib so lebensfroh wie du bist und heute trinken wir auf dich 😘"
    },
    {
      name: "Nicole Rottmann",
      img: "nicole_rottmann.png",
      text: "Lieber Michi, ich wünsche dir von ❤️ alles Liebe zum 30. Geburtstag. Wir kennen uns zwar erst seit ein paar Jahren aber gefühlt schon immer, denn du bist ein Herzensmensch ❗️ Ich hoffe du bleibst so wie du bist, so ein ehrlicher, verrückter, spaßiger, aufgeschlossener Mensch. Und ich hoffe noch auf viele weitere tolle, verrückte und abenteuerliche Unternehmungen mit dir und denk immer daran, im Notfall holt uns mein Bruder mit seinem Beamer 😉🤣 Auf dich Michi 🍷😙"
    },
    {
      name: "Jan Hegenwald",
      img: "jan_hegenwald.png",
      text: "Michi… altes Haus! Ich weiß noch ganz genau als wäre es gestern gewesen. Oben ASV durfte ich als kleiner „Schulbuh“ unter meinen Idolen der ersten Mannschaft (Michi und Michi) Medizinbälle schleppen nach bester Felix Magath Manier. Anmerkung da war ich noch 1-2 Köpfe kleiner als du 😄. Mittlerweile hat sich das Blatt gewendet und du bist geschrumpft bzw. ich gewachsen 😜. Wir spielen jetzt gemeinsam in einer Mannschaft, was mich unglaublich stolz macht und ich freu mich immer wenn du den Weg in die Heimat findest. Mit deiner offenen, ehrlichen und teilweise auch verrückten Art passt du super in unsere Truppe und „die Feierei hat oftmals keine Grenzen 😂“. In einer Saga wird berichtet man nennt dich auch den „Golden Boy“ aufgrund des letzten Bechers. Dies würde ich gerne in einem Urlaub live miterleben und hoffe auch wenn uns ein paar Jahre trennen, dass wir weiterhin so gut befreundet sind und du nicht so schnell rostest 😜 Happy Birthday Michi 😘 und Prost 🍻"
    },
    {
      name: "Marius Thomas",
      img: "marius_thomas.png",
      text: "Lieber Michi, auch ein Superstar wird leider nicht jünger. Ich wünsche dir für die Zukunft nur das Beste! Bleib so wie du bist! Ich bin schon gespannt wo du in nächster Zeit häufiger deine Sprintstärke zur Schau stellst. An die Bar oder auf dem Fußballplatz? Du bist auf jeden Fall ein WELTKLASSE TYP! Jetzt wird richtig gescheppert!"
    },
    {
      name: "Tobi Schwarz",
      img: "tobi_schwarz.png",
      text: "Hi Michi, wir kennen uns zwar noch nicht lange aber hatten schon einige geniale Tage und vor allem Nächte! Egal ob Pizza backen um 6 Uhr früh oder gemütliche 6 Bier beim Spalierstehen. Bleib wie du bist und auf weitere gute Tage! Dann funktioniert das mit dem Snus in Zukunft auch!😘"
    },
    {
      name: "Dennis Gerigk",
      img: "dennis_gerigk.png",
      text: "Happy Birthday Michi!!🥳 Wir kennen uns zwar erst seit kurzem, aber haben trotzdem schon einige Bretter gemacht. Egal ob auf dem Spielfeld, beim feiern oder um 5 Uhr Nachts Pizza machen im ASV, mit dir hat man einfach immer eine Gaudi. Bleib so wie du bist, ich hoffe wir erleben noch einige Abenteuer zusammen.😎 Dein Gerri oder Gerringer😘"
    },
    {
      name: "Verena Göbhardt",
      img: "verena_goebhardt.png",
      text: "Auch ich wünsch dir zu deinem zehnten 20ten Geburtstag von Herzen nur das Beste! Auch wenn du bereits die ein oder andere BeerPong-Niederlage erleiden musstest, bist du immer noch ein weltoffener, humorvoller und meistens auch emphatischer Mensch, der jede Feierlichkeit und jeden Urlaub bereichert. Bleib auch die nächsten 30 Jahre wie du bist! I believe in you ❤️"
    },
    {
      name: "Lena Körbl",
      img: "lena_koerbl.png",
      text: "Lieber Michi, ich wünsche dir alles, alles Liebe zu deinem 30. Geburtstag und nur das allerbeste für die Zukunft! Es ist schön dich als Freund zu haben, denn wenn man einfach mal sinnfrei schmarren will oder aber ein tiefgründiges Gespräch sucht, ist man bei dir immer genau richtig. Du bist mein liebster hüpfender Psycho-Disco-Zwerg 🕺🏼 und machst jede Feier besser, aber auch ein einfaches Kaffee-Date ☕️schätze ich mit dir! Bleib so wie du bist und heute trinken wir auf dich 🥂🍾 auf viele weitere Jahre 🥰"
    },
    {
      name: "Lukas Pflaum",
      img: "lukas_pflaum.png",
      text: "Lieber Michi, ich wünsche dir vom ganzen Herzen alles Gute zum 30. Geburtstag. Zwar kennen wir uns noch nicht so lange, aber ich wusste vom ersten Vollsuff an, was ein geiler Typ du bist. Die Harmonie zwischen uns wurde sofort entfacht und zusammen haben wir schon viel gelacht. Ich bin froh dich meinen Freund nennen zu dürfen. Auf viele weitere unvergessliche Nächte! Fürs Erste nehmen wir aber deinen 30. Geburtstag komplett auseinander. Bleib so wie du bist Legende! Dein Lucky"
    },
    {
      name: "Kevin Schley",
      img: "kevin_schley.png",
      text: "Servus Teilzeit-Nachber, man kennt dich vor allem als stets gut gelaunte, ehrliche Person und Feierbiest. Besonders freut es mich daher, dass wir in diesem Jahr bereits gemeinsam den Aufstieg feiern konnten, nachdem du mich in der Jugend noch trainiert hattest. Ob im Kaiser unter deiner „Aufsicht“ oder als Beer-Pong-Gegner in Kroatien, mit dir war eben schon immer alles schön Choco Loco. In diesem Sinne wünsche ich dir zum 30. alles erdenklich Gute, viel Glück und Gesundheit. Bleib so wie du bist! Gruß, Kev"
    },
    {
      name: "Dominik Schmeiduch",
      img: "dominik_schmeiduch.png",
      text: "Servus Michi, ich wünsche dir alles, alles Gute zu deinem 30. Geburtstag (Wilkommen im Club 😂😂)🍻. Mir persönlich sind natürlich unsere unvergesslichen, Legendären Ausflüge auf die Insel in ganz besonderer Erinnerung geblieben 😎🍻🥴. Bleib genau so wie du bist, du geiler Typ 😂😎👍🏻"
    }
  ];

  constructor() { }

  getFans(): Array<Fan> {
    return this.fans;
  }
}

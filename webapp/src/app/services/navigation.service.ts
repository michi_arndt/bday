import { Injectable } from '@angular/core';
import { NavItem } from '../interfaces/navItem';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  navItems: Array<NavItem> = [
    {
      name: "Home",
      linkTarget: "home",
      icon: ["fa-solid", "fa-house"],
      active: false,
      visible: false
    },
    {
      name: "Auf dich",
      linkTarget: "about",
      icon: ["fa-solid", "fa-champagne-glasses"],
      active: false,
      visible: false
    },
    {
      name: "Stärken",
      linkTarget: "skills",
      icon: ["fa-solid", "fa-thumbs-up"],
      active: false,
      visible: false
    },
    {
      name: "Fans",
      linkTarget: "fans",
      icon: ["fa-solid", "fa-users"],
      active: false,
      visible: false
    },
    {
      name: "Wünsche",
      linkTarget: "wishes",
      icon: ["fa-solid", "fa-heart"],
      active: false,
      visible: false
    }
  ];

  constructor() { }

  getNavItems(): Array<NavItem> {
    return this.navItems;
  }

  updateCurrentNav(linkTarget: string): void {
    let currentItem = this.navItems.find(item => item.linkTarget === linkTarget);
    if (!currentItem) {
      return;
    }
    this.navItems.forEach(item => {
      if (item === currentItem) {
        item.active = true;
      } else {
        item.active = false;
      }
    });
  }
}

import { Injectable } from '@angular/core';
import { Skill } from '../interfaces/skill';

@Injectable({
  providedIn: 'root'
})
export class SkillsService {

  techSkills: Array<Skill> = [
    {
      name: "Bierpong",
      description: "Immer gut für den letzten Becher.",
      level: 75
    },
    {
      name: "Saufen",
      description: "Ob Bier, Baca oder Wein - du haust alles rein.",
      level: 100
    },
    {
      name: "Frauen",
      description: "Du bist der Schreck des Finanzamts.",
      level: 90
    },
    {
      name: "Influencer",
      description: "Legendäre Insta-Pics aus der Münchener Schickeria.",
      level: 85
    },
    {
      name: "Großzügigkeit",
      description: "Mit dir an der Bar fließen die Shots.",
      level: 95
    },
    {
      name: "Fußball",
      description: "Überragender Goalgetter - ab und zu eine Hacke zu viel.",
      level: 90
    },
    {
      name: "Freundschaft",
      description: "Von Herzen einer der Besten.",
      level: 100
    },
    {
      name: "Essen",
      description: "Ein Zustelltisch sagt mehr als 1000 Worte.",
      level: 100
    },
    {
      name: "Fitness",
      description: "Zeitweise stark. Die Konstanz lässt ab und an zu wünschen übrig.",
      level: 75
    },
    {
      name: "Songtexte",
      description: "Leider keine Stärke. Trotzdem immer witzig, deswegen mitaufgeführt.",
      level: 1
    },
    {
      name: "Taktgefühl beim Klatschen",
      description: "Auch sehr ausbaufähig. Dennoch stets bemüht.",
      level: 10
    },
    {
      name: "Loyalität",
      description: "Du kümmerst dich um die, die dir am Herzen liegen.",
      level: 100
    },
    {
      name: "Trash(-TV) Know-how",
      description: "Wenn's ums Reality-TV geht, hast du den Durchblick.",
      level: 95
    },
    {
      name: "Einfallsreichtum",
      description: "Geile, lustige Ideen - Kannst du.",
      level: 90
    },
    {
      name: "Macher",
      description: "Von DJ-Party bis 1-Tages Malle Trip. Der Zug hat bei dir keine Bremse.",
      level: 100
    }
  ];

  constructor() { }

  getSkills(): Array<Skill> {
    return this.techSkills;
  }
}

import { NavigationService } from './services/navigation.service';
import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { NavItem } from './interfaces/navItem';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'webapp';

  navItems: Array<NavItem> = [];
  menuClosed = true;

  constructor(private navService: NavigationService) {
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.navItems = this.navService.getNavItems();
    });
  }

  onUpdateNavItems(component): void {
    setTimeout(() => {
      this.navService.updateCurrentNav(component.currentNavTarget);
    });
  }

  toggleMenu(): void {
    let timer = 100;

    if (!this.menuClosed) {
      this.navItems.forEach(item => {
        item.visible = false;
      });
    } else {
      this.navItems.forEach(item => {
        setTimeout(() => {
          item.visible = true;
        }, timer);
        timer += 100;
      });
    }
    this.menuClosed = !this.menuClosed;
  }
}

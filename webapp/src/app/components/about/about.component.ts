import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  currentNavTarget: string = "";
  reasons: Array<string> = [
    'Du fährst sogar für die Zweite Mannschaft nach Hause.',
    'Du machst jede Feier zu einem Fest.',
    'Du gibst alles, damit deine Freunde eine gute Zeit haben.',
    'Du bist für jeden Scheiß zu haben.',
    'Du weißt wahre Freundschaft zu schätzen.',
    'Du bist ein Motivator und steckst voller Ideen.',
    'Mit dir wird jeder Trip unvergesslich.',
    'Auf deinen ehrlichen Rat kann man sich verlassen.',
    'Mit dir wird es nie langweilig und die Zeit verfliegt.',
    'Du bist ein Mensch, der gute Laune verbreitet.',
    'Mit dir geht Lieferando nie bankrott.',
    'Du bist ein Vorbild für unsere Fußballjugend. #partyhard'
  ];

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.currentNavTarget = this.activatedRoute.snapshot.url[0].path;
    window.scrollTo(0, 0);
  }

}

import { Component, Input, OnInit } from '@angular/core';
import { NavItem } from 'src/app/interfaces/navItem';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  @Input() navItems!: Array<NavItem>;

  constructor() { }

  ngOnInit(): void {
  }

}

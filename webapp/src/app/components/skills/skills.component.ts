import { SkillsService } from './../../services/skills.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Skill } from 'src/app/interfaces/skill';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {

  currentNavTarget: string = "";
  skills?: Array<Skill>;

  constructor(private activatedRoute: ActivatedRoute, private skillService: SkillsService) { }

  ngOnInit(): void {
    this.currentNavTarget = this.activatedRoute.snapshot.url[0].path;
    this.skills = this.skillService.getSkills();
    window.scrollTo(0, 0);
  }

}

import { FanService } from './../../services/fan.service';
import { Fan } from './../../interfaces/fan';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-fan',
  templateUrl: './fan.component.html',
  styleUrls: ['./fan.component.scss']
})
export class FanComponent implements OnInit {

  currentNavTarget: string = "";
  imgSrcPath: string = '../../../assets/images/fans/';
  fans?: Array<Fan>;

  constructor(private activatedRoute: ActivatedRoute, private fanService: FanService) { }

  ngOnInit(): void {
    this.currentNavTarget = this.activatedRoute.snapshot.url[0].path;
    this.fans = this.fanService.getFans();
    window.scrollTo(0, 0);
  }

  getImgSrc(fan: Fan): string {
    return `assets/images/fans/${fan.img}`;
  }
}

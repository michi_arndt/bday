import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appSetIndicator]'
})
export class SetIndicatorDirective {

  constructor(private el: ElementRef, private renderer: Renderer2) {
    console.log({ el });
    this.renderer.setStyle(this.el.nativeElement, 'width', `${50}%`);
  }

}

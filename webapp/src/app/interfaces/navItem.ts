export interface NavItem {
    name: string;
    linkTarget: string;
    icon: Array<string>;
    active: boolean;
    visible: boolean;
}
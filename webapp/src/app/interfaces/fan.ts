export interface Fan {
    name: string;
    img: string;
    text: string;
}